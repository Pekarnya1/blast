
# BLAST Game Prototype

BLAST is a game prototype where you can remove blocks by clicking on them and get a victory message when you have successfully removed all blocks.

## Installation

To install the game, run the following command:




## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/Pekarnya1/blast
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run server
```


## Acknowledgements

 - [Awesome Readme Templates](https://awesomeopensource.com/project/elangosundar/awesome-README-templates)
 - [Awesome README](https://github.com/matiassingers/awesome-readme)
 - [How to write a Good readme](https://bulldogjob.com/news/449-how-to-write-a-good-readme-for-your-github-project)


## Authors

- [@Pekarnya1](https://gitlab.com/Pekarnya1)




## Roadmap

- Additional browser support

- Add more integrations
- Add more levels
- Add more Characters


## Tech Stack

**Client:** PIXI.js, Howl,

**Server:** Node


## 🛠 Skills
Javascript, HTML, PIXI.js, OOP


## Badges


![Creative Commons Legal Code](https://upload.wikimedia.org/wikipedia/commons/7/7a/CreativeCommons_logo_trademark.svg)


## FAQ

### Is it final version?

No, this is just prototype, but it can be scaled up to the big project.


### What we can add into the game?

To expand the game, we need to add more assets and define the rules for building new levels.

### Can I contribute this project?

Yes for sure, just drop me a line:
litvinenkogosa4@gmail.com


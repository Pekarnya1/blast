import {Application} from 'pixi.js';
import {assets} from './textureMap.js';
import {createBackground, animateBackground} from './puzle/backgroundConf.js';
import {configPointer, backSoundPlay} from './puzle/conf/gameConfig.js';
import '../assets/styles/pointerStyles.css';
import {Board} from './puzle/game/board.js';


// Start playing the back sound
// N.B it is not necessary to place sound into the game loop
backSoundPlay;

const app = new Application({
  width: window.innerWidth,
  height: window.innerHeight,
});

// Hide the overflow
document.body.style.overflow = 'hidden';

app.screen.width = window.innerWidth;

document.body.appendChild(app.view);
app.stage.interactive = true;

// eslint-disable-next-line require-jsdoc
function addAssetsToLoader(assets) {
  assets.forEach((value) => app.loader.add(value.name, value.url));
}

// preload assets before game starts
assets.forEach((asset) => addAssetsToLoader(asset));


const runGame = () => {
  const background = createBackground(window.innerWidth);

  console.log(background);

  app.stage.addChild(background.view);


  // Configuration of the custom pointer
  const pointer = configPointer('default', app);
  pointer.setPointer();

  console.log(app.stage);

  animateBackground(background, app);
  const board = new Board(app);
};
app.loader.load(runGame);

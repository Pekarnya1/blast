import {soundsEffects} from '../conf/gameConfig.js';
import {TextureCreator} from '../graphics.js';
import * as PIXI from 'pixi.js';

/**
 * Describes the rules and methods for
 * working with the field (single square) instance on the board
 * @param {number} row current row of the board sector.
 * @param {number} col current column of the board sector.
 * @param {string} spriteName name of the sprite
 * N.B I`m using TextureMap but if you dont, you have to pass
 * the relative or absolute path to the image!
 * @example
 * // Add sprite of cube:
 * const cube = new Cube(10, 10, 'blue_cube');
  app.stage.addChild(cube.sprite);
*/
export class Cube {
  // eslint-disable-next-line require-jsdoc
  constructor(row, col, spriteName) {
    this.spriteName = spriteName;
    this.color = spriteName;
    this.row = row;
    this.col = col;
    this.textureObj = new TextureCreator(this.spriteName, 50, 50);
    this.sprite = this.textureObj.createSprite(false);
    this.sprite.position.set(this.position.x, this.position.y);
    this.sprite.interactive = true;
    this.sprite.on('mouseover', this._onHover.bind(this));
    this.sprite.on('mouseout', this._outHover.bind(this));
    // this.sprite.on('pointerdown', this._onPointerDown.bind(this));
    // console.log(`${this.sprite}`);
  }

  /**
 * Get the position of the cube
*/
  get position() {
    return {
      x: this.col * 50,
      y: this.row * 50,
    };
  }


  /**
   * Describes logics that aplies when pointerdown
   * on the sprite.
   */
  _onPointerDown() {
    this.sprite.destroy();
  }

  /**
   * Describes filter that aplies when pointer is hovering
   * on the sprite.
   */
  _onHover() {
    // transparent filterGreen.colorTone(22, 55, 5, 4, true)
    // this.sprite.anchor.set(0.5);
    soundsEffects.sword.play();
    const filterOnHover = new PIXI.filters.ColorMatrixFilter();
    this.sprite.filters = [filterOnHover];
    filterOnHover.kodachrome(true);
  }

  /**
   * Describes filter that aplies when pointer is hovering
   * out the sprite.
   */
  _outHover() {
    // transparent filterGreen.colorTone(22, 55, 5, 4, true)
    // this.sprite.anchor.set(0.5);
    this.sprite.filters = [];
  }


  // eslint-disable-next-line require-jsdoc
  setTile(tile) {
    this.tile = tile;
    tile.field = this;
    tile.setPosition(this.position);
  }
}

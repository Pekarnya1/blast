import {Board} from './board';

/**
 * Represents the logics for finding
 * the neighbors of clicked elements
 *
 */
export class Adjacents extends Board {
  /**
   */
  constructor() {
    super();
  }

  /**
   * returns the index of the specified cube in the
   * fields array
   */
  get indexCube() {
    return (cube) => {
      return cube.col + cube.row * this.rows;
    };
  }

  // eslint-disable-next-line require-jsdoc
  indexBotom(cube) {
    if (cube.row + 1 == this.rows) {
      return null;
    } else {
      return this.indexCube(cube) + this.rows;
    }
  }

  // eslint-disable-next-line require-jsdoc
  indexUpper(cube) {
    if (cube.row == 0) {
      return null;
    } else {
      return this.indexCube(cube) - this.rows;
    }
  }

  // eslint-disable-next-line require-jsdoc
  indexLeft(cube) {
    if (cube.col == 0) {
      return null;
    } else {
      return this.indexCube(cube) - 1;
    }
  }

  // eslint-disable-next-line require-jsdoc
  indexRight(cube) {
    if (cube.col + 1 == this.rows) {
      return null;
    } else {
      return this.indexCube(cube) + 1;
    }
  }
}

import {Container, Text} from 'pixi.js';
import {boardConfig, soundsEffects} from '../conf/gameConfig';
import {Cube} from './field';
import {blockMap} from '../../textureMap';
import {createBoardBackground} from '../backgroundConf';
import {explosionAnimation, textStyle} from '../conf/gamestyle';
import {Character} from './character';

/**
 * Representation of the board instance with it container
 */
export class Board {
  // eslint-disable-next-line require-jsdoc
  constructor(app) {
    this.character = new Character();
    this.characterContainer = this.character.create();
    this.app = app;
    this.app.stage.addChild(this.characterContainer);
    this.container = new Container();
    this.container.sortableChildren = true;
    this.fields = [];
    this.spritesField = [];
    this.rows = boardConfig.rows;
    this.cols = boardConfig.cols;
    this.K = boardConfig.k;
    this.create();
  }

  // eslint-disable-next-line require-jsdoc
  create() {
    this.createFields();
    this.#createBackground();
    this.container.addChild(this.boardContainer);
    this.#adjustBoardPosition();
  }

  // eslint-disable-next-line require-jsdoc
  #createBackground() {
    this.backgroundObj = createBoardBackground(this.width, this.height);
    this.boardContainer = this.backgroundObj.view;
    this.boardContainer.zIndex -= 1;
  }

  // eslint-disable-next-line require-jsdoc
  createFields() {
    for (let row = 0; row < this.rows; row++) {
      for (let col = 0; col < this.cols; col++) {
        this.createField(row, col);
      }
    }
  }

  // eslint-disable-next-line require-jsdoc
  createField(row, col, isUpdate = false) {
    const cube = new Cube(row, col, this.#randomizeBlock());
    if (isUpdate) {
      this.fields[this.indexCube(cube)] = cube;
    } else {
      this.fields.push(cube);
    }

    this.container.addChild(cube.sprite);
    this.#adjustPosition();
    // add the sprite click event listeners
    cube.sprite.interactive = true;
    cube.sprite.on('click', () => {
      this._onclick(cube);
    });
    this.app.stage.addChild(this.container);
    // console.log(this.fields);
  }

  /**
   * Method for creating a new sprites
   * into the container depending on indices of blank positions
   */
  updateField() {
    let index = Object.values(this.nullIndices())[0];
    while (typeof index === 'number') {
      if (index <= this.cols) {
        const row = Math.floor(index / this.cols);
        const col = index % this.cols;
        this.createField(row, col, true);
      } else {
        this.tileFall(index);
      }
      index = Object.values(this.nullIndices())[0];
    }
    this.updateFieldPositions();
  }


  // eslint-disable-next-line require-jsdoc
  updateFieldPositions() {
    for (let i = 0; i < this.fields.length; i++) {
      const cube = this.fields[i];
      cube.row = Math.floor(i / this.cols);
      cube.col = i % this.cols;
    }
  }


  // eslint-disable-next-line require-jsdoc
  tileFall(index) {
    const cubeAboveIndex = this._blankIndexAbove(index);
    const cubeFall = this.fields[cubeAboveIndex];
    const newRow = index % this.cols;
    cubeFall.row = newRow;
    this.fields[index] = cubeFall;
    this.fields[cubeAboveIndex] = null;
    cubeFall.sprite.y += cubeFall.sprite.height;
  }

  // eslint-disable-next-line require-jsdoc
  _blankIndexAbove(blankIndex) {
    while (this.fields[blankIndex] === null) {
      blankIndex -= this.cols;
      if (blankIndex <= this.cols) {
        return blankIndex;
      } else {
        return blankIndex;
      }
    }
  }


  // eslint-disable-next-line require-jsdoc
  _blankRows(blankIndices) {
    const nullValues = Object.values(blankIndices);
    const blankRows = [];
    const blankCols = [];
    for (const val of nullValues) {
      const row = Math.floor(val / this.cols);
      const col = val % this.cols;
      blankRows.push(row);
      blankCols.push(col);
    }
    return {rows: blankRows, cols: blankCols};
  }

  // eslint-disable-next-line require-jsdoc
  nullIndices() {
    const indicesUpdate = this.fields.reduce((acc, val, index) => {
      if (val === null) {
        acc.push(index);
      }
      return acc;
    }, []);

    return indicesUpdate;
  }

  /**
   * Describes the logics when sprite is clicked
   * @param {Cube} cube - Cube instance
   */
  _onclick(cube) {
    const indexCube = this.indexCube(cube);
    const clickedColor = cube.color;
    const neighborIndices = [
      this.indexBotom(cube),
      this.indexRight(cube),
      this.indexUpper(cube),
      this.indexLeft(cube),
    ].filter((index) => index !== null);

    const cubesToRemove = [indexCube];
    const cubesToCheck = neighborIndices.slice();

    while (cubesToCheck.length > 0) {
      const currentIndex = cubesToCheck.shift();
      if (cubesToRemove.includes(currentIndex)) {
        continue;
      }
      const currentCube = this.fields[currentIndex];
      if (currentCube.color === clickedColor) {
        cubesToRemove.push(currentIndex);
        const currentNeighborIndices = [
          this.indexBotom(currentCube),
          this.indexRight(currentCube),
          this.indexUpper(currentCube),
          this.indexLeft(currentCube),
        ].filter((index) => index !== null);
        cubesToCheck.push(...currentNeighborIndices);
      }
    }

    // Change this line to param K!!!!
    if (cubesToRemove.length >= this.K) {
      cubesToRemove.forEach((index) => {
        const cubeToRemove = this.fields[index];
        this.deleteTile(cubeToRemove);
        this.fields[index] = null;
        console.log(this.fields);
      });
    }
    this.updateField();
  }

  /**
   * Remooves the instance of tile from PIXI.js container
   * @param {Cube} tileToRemove
   * @example
   * board = new Board(app);
   * board.removeTile(tileToRemove);
   * // or
   * this.deleteTile(tileToRemove);
   */
  deleteTile(tileToRemove) {
    this.character.updateHP(1);
    if (this.character.HP <= 0) {
      this.victory();
    }
    const x = tileToRemove.sprite.x - tileToRemove.sprite.width;
    const y = tileToRemove.sprite.y - tileToRemove.sprite.width;
    explosionAnimation(this.container, x, y);
    this.container.removeChild(tileToRemove.sprite);
    soundsEffects.explosion.play();
  }

  // eslint-disable-next-line require-jsdoc
  victory() {
    const message = new Text(
        'You won!', textStyle);
    message.x = 100;
    message.y = 100;
    this.app.stage.removeChildren(0, 1199);
    delete this.fields;

    this.app.stage.addChild(message);
  }


  // eslint-disable-next-line require-jsdoc
  #adjustBoardPosition() {
    this.boardContainer.x -= this.width * 0.10;
    this.boardContainer.y -= this.height * 0.10;
  }

  // eslint-disable-next-line require-jsdoc
  #adjustPosition() {
    // have to change this to sprite size
    this.fieldSize = 50; // this.fields[0].sprite.width;
    this.width = this.cols * this.fieldSize;
    this.height = this.cols * this.fieldSize;
    this.container.x = (this.app.screen.width - this.width
    ) / 2 + this.fieldSize / 2;
    this.container.y = (this.app.screen.height - this.height
    ) * 0.618 + this.fieldSize * 0.618;
  }

  // eslint-disable-next-line require-jsdoc
  #randomizeBlock() {
    const randomIndex = Math.floor(
        Math.random() * blockMap.cubesColored.length);
    const randomName = blockMap.cubesColored[randomIndex].name;
    return randomName;
  }

  /**
   * returns the index of the specified cube in the
   * fields array
   */
  get indexCube() {
    return (cube) => {
      return cube.col + cube.row * this.rows;
    };
  }

  // eslint-disable-next-line require-jsdoc
  indexBotom(cube) {
    if (cube.row + 1 == this.rows) {
      return null;
    } else {
      return this.indexCube(cube) + this.rows;
    }
  }

  // eslint-disable-next-line require-jsdoc
  indexUpper(cube) {
    if (cube.row == 0) {
      return null;
    } else {
      return this.indexCube(cube) - this.rows;
    }
  }

  // eslint-disable-next-line require-jsdoc
  indexLeft(cube) {
    if (cube.col == 0) {
      return null;
    } else {
      return this.indexCube(cube) - 1;
    }
  }

  // eslint-disable-next-line require-jsdoc
  indexRight(cube) {
    if (cube.col + 1 == this.rows) {
      return null;
    } else {
      return this.indexCube(cube) + 1;
    }
  }
}

// Transparent sprite
// this.background = new TextureCreator('board_background', 400, 400);
// this.backSprite = this.background.createSprite(this.background);
// const filterGreen = new filters.ColorMatrixFilter();
// this.backSprite.filters = [filterGreen];
// filterGreen.colorTone(22, 55, 0, 0, true);
// this.container.addChild(this.backSprite);


import * as PIXI from 'pixi.js';
import {textStyle} from '../conf/gamestyle';
// eslint-disable-next-line require-jsdoc
export class Character {
  // eslint-disable-next-line require-jsdoc
  constructor() {
    this.HP = 1000;
    this.container = new PIXI.Container();
  }

  // eslint-disable-next-line require-jsdoc
  create() {
    this.animate();
    this.setHP();
    return this.container;
  }

  // eslint-disable-next-line require-jsdoc
  animate() {
    // Load the texture atlas using fetch and JSON.parse
    fetch('assets/sprites/bird/spritesheet.json')
        .then((response) => response.json())
        .then((data) => {
          // Create an AnimatedSprite from the loaded texture atlas
          const textures = [];
          for (const key in data.frames) {
            const texture = PIXI.Texture.from(key);
            textures.push(texture);
          }
          this.animatedSprite = new PIXI.AnimatedSprite(textures);
          this.animatedSprite.width = 200;
          this.animatedSprite.height = 200;

          // Set the animation properties
          this.characterContainer = new PIXI.Container();
          this.characterContainer.addChild(this.animatedSprite);

          // Add the AnimatedSprite to the container
          this.container.addChild(this.characterContainer);
          this.animatedSprite.position.set(100, 100);
          this.animatedSprite.animationSpeed = 0.2;
          this.animatedSprite.loop = true;
          this.animatedSprite.play();
        });
  }

  // eslint-disable-next-line require-jsdoc
  setHP() {
    this.hpText = new PIXI.Text(`HP: ${this.HP}`, textStyle);
    this.hpText.x = 100;
    this.hpText.y = 20;

    this.container.addChild(this.hpText);
  }

  // eslint-disable-next-line require-jsdoc
  updateHP(numTiles) {
    this.container.removeChild(this.hpText);
    this.HP -= numTiles * 2;
    this.setHP();
  }
}

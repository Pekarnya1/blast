import {PointerBuilder} from './gamestyle.js';
import {Howl} from 'howler';

export const configPointer = (setting, app) => {
  const pointerStyle = new PointerBuilder(setting, app)
      .build();

  return pointerStyle;
};

// Board configuration
export const boardConfig = {
  rows: 7,
  cols: 7,
  k: 2,
};

// export const defaultPointer = configPointer('default');

const musicUrl = './music';

const musicSRC = [
  `${musicUrl}/storm-clouds-purpple-cat.mp3`,
  `${musicUrl}/JAZ013801.mp3`,
  `${musicUrl}/lofi1.mp3`,
];

/**
 * recursive function that play resource mp3 files
 * @param {number} i index of resource
 * @param {classList} musicSRC list of the resource files
 */
function backSound(i, musicSRC) {
  const sound = new Howl({
    src: musicSRC[i],
    preload: true,
    onend: function() {
      if ((i +1) == musicSRC.length) {
        backSound(0, musicSRC);
      } else {
        backSound(i + 1, musicSRC);
      }
    },
  });
  sound.play();
};

export const soundsEffects = {
  explosion: new Howl({src: [`${musicUrl}/sound/explosion.mp3`],
    preload: true,
    volume: 0.1}),
  sword: new Howl({src: [`${musicUrl}/sound/sword.mp3`],
    preload: true,
    volume: 0.2}),
};

// Have to rewrite that as a class
export const backSoundPlay = backSound(0, musicSRC);


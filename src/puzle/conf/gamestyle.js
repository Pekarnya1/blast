import * as PIXI from 'pixi.js';
import {Texture, AnimatedSprite, Container} from 'pixi.js';

/**
 * Create pointer style.
 * N.B: Call it only via {BackgroundBuilder} constructor
 * @param {object} cursorInstance - object that contains
 * @param {Object} app current aplication
 * the keys of layers and the name from textureMap
 */
export class Pointer {
  // eslint-disable-next-line require-jsdoc
  constructor(setting, app) {
    this.setting = setting;
    this.app = app;
  }

  /**
 * Set the pointer style depend on setting preference
 */
  setPointer() {
    if (this.setting === 'default') {
      this.#setDefaultPointer();
    }
  }
  /**
 * Set default pointer style.
 * via just changing the document.body style.
 */
  #setDefaultPointer() {
    document.body.classList.add('default-cursor');
  }
}

/**
  * Continuously builds the object of the pointer
  * configuration
  * @param {string} setting - string defenition of
  * @param {Object} app current application
  * the configuration
  */
export class PointerBuilder {
  // eslint-disable-next-line require-jsdoc
  constructor(setting, app) {
    this.app = app;
    this.setting = setting;
    this.cursorInstance = {};
  }

  /**
  * Continuously builds the object of the pointer
  * configuration
  * @param {string} object - The string containing the name of the object
  * to build.
  * @param {string} name - The name of the object thar is
  * already defined in texture map.-
  * @return {object} Sprite object with key to manipulate
  * and value to adress co bsckground map.
  */

  /**
  * Creates final build of the Pointer object.
  * @return {Pointer} new Pointer.
  */
  build() {
    return new Pointer(this.setting, this.app);
  }
}

// eslint-disable-next-line require-jsdoc
export function explosionAnimation(container, x, y) {
  // Load the texture atlas using fetch and JSON.parse
  fetch('assets/sprites/explosions/spritesheet.json')
      .then((response) => response.json())
      .then((data) => {
      // Create an AnimatedSprite from the loaded texture atlas
        const textures = [];
        for (const key in data.frames) {
          const texture = PIXI.Texture.from(key);
          textures.push(texture);
        }
        const animatedSprite = new PIXI.AnimatedSprite(textures);

        // Set the animation properties
        const explosionContainer = new Container();
        explosionContainer.addChild(animatedSprite);

        // Add the AnimatedSprite to the container
        container.addChild(explosionContainer);
        animatedSprite.position.set(x, y);
        animatedSprite.animationSpeed = 0.2;
        animatedSprite.loop = false;
        animatedSprite.play();
        animatedSprite.onComplete = () => {
          container.removeChild(explosionContainer);
        };
      });
}

export const textStyle = new PIXI.TextStyle({
  fontFamily: 'Arial',
  fontSize: 36,
  fontStyle: 'italic',
  fontWeight: 'bold',
  fill: ['#ffffff', '#00ff99'], // gradient
  stroke: '#4a1850',
  strokeThickness: 5,
  dropShadow: true,
  dropShadowColor: '#000000',
  dropShadowBlur: 4,
  dropShadowAngle: Math.PI / 6,
  dropShadowDistance: 6,
  wordWrap: true,
  wordWrapWidth: 440,
  lineJoin: 'round',
});



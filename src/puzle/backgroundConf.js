
import {BackgroundBuilder} from './graphics.js';

export const createBackground = (width) => {
  const background = new BackgroundBuilder(width)
      .addObject('baseTexture', 'blue_sky')
      .addObject('animatedObject', 'clouds')
      .addObject('baseGround', 'land')
      .addObject('backTerrarian', 'back_mountains')
      .addObject('backObjectLayer', 'back_tree')
      .addObject('frontObjectLayer', 'front_tree')
      .addObject('backGrass', 'grass')
      .addObject('frontGround', 'front_grass')
      .build();

  return background;
};

export const createBackgroundTest = (width) => {
  const background = new BackgroundBuilder(width)
      .addObject('baseTexture', 'blue_sky')
      .addObject('animatedObject', 'clouds')
      .addObject('backTerrarian', 'back_mountains')
      .addObject('backGrass', 'grass')
      .build();

  return background;
};

export const animateBackgroundTest = (background, app) => {
  app.ticker.add(() => {
    background._spriteMap.animatedObject.tilePosition.x -= (Math.sin(
        4))/6;
    background._spriteMap.backTerrarian.tilePosition.x += 0.14;
    background._spriteMap.backGrass.tilePosition.x += 0.56;
  });
};

export const animateBackground = (background, app) => {
  app.ticker.add(() => {
    background._spriteMap.animatedObject.tilePosition.x += 0.02;
    background._spriteMap.animatedObject.tilePosition.y += (Math.cos(
        background._spriteMap.animatedObject.tilePosition.x))/6;
    background._spriteMap.backTerrarian.tilePosition.x += 0.04;
    background._spriteMap.backObjectLayer.tilePosition.x += 0.08;
    background._spriteMap.frontObjectLayer.tilePosition.x += 0.16;
    background._spriteMap.backGrass.tilePosition.x += 0.32;
    background._spriteMap.frontGround.tilePosition.x += 0.64;
  });
};

export const createBoardBackground = (width, height) => {
  const backgroundObj = new BackgroundBuilder({
    width: width * 1.20,
    height: height * 1.20,
    isTiling: false,
    isFit: false,
    isTransparent: true,
  })
      .addObject('board_black', 'board_black')
      .build();
  return backgroundObj;
};

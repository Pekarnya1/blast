
import {Container, Sprite,
  Texture, TilingSprite, filters, AnimatedSprite} from 'pixi.js';

/**
 * Create container background filled with specified sprites.
 * N.B: Call it only via {BackgroundBuilder} constructor
 * @param {object} spriteInstances - object that contains
 * the keys of layers and the name from textureMap
 * @return {Container} Container filled with the sprites.
 */
export class Background {
  // eslint-disable-next-line require-jsdoc
  constructor(spriteInstances, width, height, isFit, isTiling, isTransparent) {
    // Create a new background container
    this._view = new Container();

    this.width = width;
    this.height = height;

    this.spriteInstances = spriteInstances;

    this.isFit = isFit;
    this.isTiling = isTiling;
    this.isTransparent = isTransparent;

    this.instancesKeys = Object.keys(this.spriteInstances);
    console.log(this.instancesKeys);

    this._textureMap = {};
    this._spriteMap = {};

    this.#texturePack();
  }

  /**
 * Method to collect sprites and place them
 * into the container as child.
 * This method is using exemplars of TextureCreator class
 * @param {number} width width of the sprite, default is equal to
 * width of the screen.
 * @param {number} height height of the sprite, default is equal to 324.
 */
  #texturePack() {
    this.instancesKeys.forEach((key) => {
      this._textureMap[key] = new TextureCreator(
          this.spriteInstances[key],
          this.width,
          this.height,
      );
      if (this.isTiling) {
        this._spriteMap[key] = this._textureMap[key].createTilingSprite(
            this.isFit, this.isTransparent);
      } else {
        this._spriteMap[key] = this._textureMap[key].createSprite(
            this.isFit, this.isTransparent);
      }
      this._view.addChild(this._spriteMap[key]);
    });
  }


  /**
  * Create sprite instance depends on flag value.
  * @return {Container} Container filled with sprites.
  */
  get view() {
    // Add the sprites into the container
    return this._view;
  }
}

/**
  * Helps to configure the background via the builder
  * pattern.
  * @param {number} window.Width - size of the window.
  */
export class BackgroundBuilder {
  /**
   * @param {Object} options - options for the background.
   */
  constructor(options = {}) {
    const {
      width = window.innerWidth,
      height = 324,
      isFit = true,
      isTiling = true,
      isTransparent = false,
    } = options;

    this.width = width;
    this.height = height;
    this.spriteInstances = {};
    this.isFit = isFit;
    this.isTiling = isTiling;
    this.isTransparent = isTransparent;
  }

  /**
  * Continuously builds the object of the background
  * configuration
  * @param {string} object - The string containing the name of the object
  * to build.
  * @param {string} name - The name of the object thar is
  * already defined in background map.-
  * @return {object} Sprite object with key to manipulate
  * and value to adress co bsckground map.
  * @example
  * // add new background object N.B name parametr is used from texture map!!!
  * // You can  name object name whatever you want.
  * const background = new BackgroundBuilder(width)
      .addObject('baseTexture', 'blue_sky')
  */
  addObject(object, name) {
    this.spriteInstances[object] = name;
    return this;
  }

  /**
  * Creates final build of the background object.
  * @return {Background} new Background.
  */
  build() {
    return new Background(this.spriteInstances,
        this.width,
        this.height,
        this.isFit,
        this.isTiling,
        this.isTransparent);
  }
}

/**
  * Wrapper for the PIXI sprite and texture classes.
  * @param {string} spriteName - The string containing
  * the name of the object
  * @param {number} width - width of the sprite.
  * @param {number} height - height of the sprite
  * @return {object} Sprite object with key to manipulate
  * and value to adress co bsckground map.
  * @example
  * // texture of the class object:
  * const textureObj = new TextureCreator(
  *   'path/to/file.png', 10, 10);
  * const texture = textureObj.texture;
  */
export class TextureCreator {
  // eslint-disable-next-line require-jsdoc
  constructor(spriteName, width = 50, height = 50, texturesArray = []) {
    this.spriteContainer = new Container();
    this.spriteName = spriteName;
    // console.log(this.spriteName);

    this.texture = Texture.from(this.spriteName);
    this.width = width;
    this.height = height;
  }

  /**
 * Create sprite instance depends on flag value.
 * @param {boolean} isFit - The switcher beetween sprites that
 * @param {boolean} isTransparent - Should the sprite be transparent?
 * default: false
 * have to fit the screen
 * @return {Sprite} Sprite instance.
 * @example
 * // sprite of the object that fits the screen
 * const object = new TextureCreator(
  *   'path/to/file.png', 10, 10);
* const sprite = object.createSprite(true);
 */
  createSprite(isFit, isTransparent = false) {
    const sprite = new Sprite(this.texture);

    // Check if the sprite shoud fit the screen
    if (isFit) {
      // document.body.clientWidth.
      sprite.width = document.body.clientWidth;
      sprite.height = document.body.clientHeight;
    }

    if (isTransparent) {
      const filter = new filters.ColorMatrixFilter();
      sprite.filters = [filter];
      filter.colorTone(2, 5, 0, 110, true);
    }

    sprite.width = this.width;
    sprite.height = this.height;

    sprite.position.set(0, 0);
    return sprite;
  }

  /**
 * Create tailing sprite.
 * N.B. In dev version to prevent some bugs
 * when tiling sprite repeating in all axis directions
 * Method use sprite.scale.set
 * @param {boolean} isFit Should the sprite fit the screen?
 * @return {TilingSprite} examplar of the PIXI.TilingSprite.
 */
  createTilingSprite(isFit) {
    const tilingSprite = new TilingSprite(
        this.texture,
        this.width,
        this.height,
    );
    tilingSprite.position.set(0, 0);
    if (isFit) {
      tilingSprite.scale.set(
          document.body.clientHeight/this.height,
      );
    }
    return tilingSprite;
  }

  /**
   * Creates an animated sprite
   * @param {Array} texturesArray - The array of sprites to be animated
   * @return {AnimatedSprite} An animated sprite instance of PIXI.js
   */
  createAnimatedSprite(texturesArray) {
    const animatedSprite = new AnimatedSprite(texturesArray);
    return animatedSprite;
  }
}

